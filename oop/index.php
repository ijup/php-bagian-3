<?php

    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');


    $sheep = new animal('shaun');
    echo "Name : " . $sheep->name . "<br>";
    echo "Leg : " . $sheep->legs . "<br>";
    echo "Cold blooded : " . $sheep->cold_blooded . "<br><br>";   

    $kodok = new Frog('buduk');
    echo "Name : " . $kodok->name . "<br>";
    echo "Leg : " . $kodok->legs . "<br>";
    echo "Cold blooded : " . $kodok->cold_blooded . "<br>";   
    echo "Jump : " . $kodok->jump() . "<br>";

    $sungokong = new Ape('kera sakti');
    echo "Name : " . $sungokong->name . "<br>";
    echo "Leg : " . $sungokong->legs . "<br>";
    echo "Cold blooded : " . $sungokong->cold_blooded . "<br>";   
    echo "Yell : " . $sungokong->yell() . "<br>";

?>